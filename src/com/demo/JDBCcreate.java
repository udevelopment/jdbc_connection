package com.demo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCcreate {
	// static Connection conn;

	public static void main(String[] args) {

		JDBCcreate jdbCcreate = new JDBCcreate();
		jdbCcreate.doTest();

	}

	private void doTest() {
		doSelectTest();

		doInserTest();
		doSelectTest();

		doUpdateTest();
		doSelectTest();

		doDeletetest();
		doSelectTest();

	}

	private void doSelectTest() {
		// TODO Auto-generated method stub
		JDBCConnection jdbcConnection = new JDBCConnection();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "select*from sampledatabase.signup";
		System.out.print("\n[Performing SELECT] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				String username = rs.getString("username");
				String email = rs.getString("email");
				System.out.println(username + "   " + email);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			jdbcConnection.closeConnection(conn, stmt, rs);
		}
	}

	private void doDeletetest() {
		JDBCConnection jdbcConnection = new JDBCConnection();
		Connection conn = null;
		Statement stmt = null;
		// TODO Auto-generated method stub
		System.out.print("\n[Performing DELETE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("DELETE FROM sampledatabase.signup WHERE idsignup='3'");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

	private void doUpdateTest() {
		JDBCConnection jdbcConnection = new JDBCConnection();
		Connection conn = null;
		Statement stmt = null;
		// TODO Auto-generated method stub
		System.out.print("\n[Performing UPDATE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE sampledatabase.signup SET username='vin diesel' WHERE idsignup='2'");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

	private void doInserTest() {
		JDBCConnection jdbcConnection = new JDBCConnection();
		Connection conn = null;
		Statement stmt = null;
		// TODO Auto-generated method stub
		System.out.print("\n[Performing INSERT] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("INSERT INTO sampledatabase.signup "
					+ "VALUES (3,'vin@gmail.com', 'VInnie', 'Test@123', 6565565656,500)");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

}
